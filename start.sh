#!/bin/bash

set -eu

readonly ARTISAN="sudo -E -u www-data php /app/code/artisan"

mkdir -p /run/cachethq/bootstrap-cache /run/cachethq/logs /run/cachethq/cache /run/cachethq/framework-cache /run/cachethq/sessions
mkdir -p /app/data/database-backups
chown -R www-data:www-data /run/cachethq /app/data

if [[ ! -f /app/data/.initialized ]]; then
    echo "=> First run"

    mkdir -p /app/data/storage
    cp -R /app/code/storage.template/* /app/data/storage
    cp /app/pkg/env.production /app/data/env

    echo "=> Generating app key"
    $ARTISAN key:generate --no-interaction

    echo "=> Run migrations and seed database"
    $ARTISAN app:install

    touch /app/data/.initialized
else
    echo "=> Existing installation. Running migration script"
    $ARTISAN app:update
fi

# sessions, logs and cache
[[ -d /app/data/storage/framework/sessions ]] && rm -rf /app/data/storage/framework/sessions
ln -sf /run/cachethq/sessions /app/data/storage/framework/sessions
rm -rf /app/data/storage/framework/cache && ln -s /run/cachethq/framework-cache /app/data/storage/framework/cache
rm -rf /app/data/storage/logs && ln -s /run/cachethq/logs /app/data/storage/logs

# clear cached stuff under /app/data/storage/framework
echo "=> Clearing cache"
$ARTISAN view:clear
$ARTISAN cache:clear

echo "=> Starting Apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

